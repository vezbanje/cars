import { SharedService } from './services/shared/shared.service';
import { CarsService } from './services/cars/cars.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CarsListComponent } from './components/cars-list/cars-list.component';
import { CarComponent } from './components/car/car.component';
import { HeaderSearchComponent } from './components/header-search/header-search.component';
import { HeaderComponent } from './components/header/header.component';
import { SelectedCarsComponent } from './components/selected-cars/selected-cars.component';

@NgModule({
  declarations: [
    AppComponent,
    CarsListComponent,
    CarComponent,
    HeaderSearchComponent,
    HeaderComponent,
    SelectedCarsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [CarsService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
