import { SharedService } from './../services/shared/shared.service';

import { Observable} from 'rxjs/Rx';

export class Car {

    private carObs: Observable<any>;

    constructor(
        private name: string,
        private description: string,
        private id: number,
        private speed: number,
        private image: string,
        public travelledDistanceInProcent: number = 0,
    ) {}

    getName() {
        return this.name;
    }

    getId() {
        return this.id;
    }

    getSpeed() {
        return this.speed;
    }

    getCarObs() {
        return this.carObs;
    }

    setCarObs(obs: Observable<any>) {
        this.carObs = obs;
    }
}
