import { Observable} from 'rxjs/Rx';

export class TrafficLight {

    private lightsObs: Observable<any>;

    constructor(public position: number, public duration: number, public color: string = 'red') {}

    getLightsObs() {
        return this.lightsObs;
    }

    setLightsObs(obs: Observable<any>) {
        this.lightsObs = obs;
    }
}

