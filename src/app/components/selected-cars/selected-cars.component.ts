import { SpeedLimit } from './../../models/speed-limit.model';
import { TrafficLight } from './../../models/traffic-light.model';
import { SharedService } from './../../services/shared/shared.service';
import { Car } from './../../models/car.model';
import { CarsService } from './../../services/cars/cars.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert2';
import { Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-selected-cars',
  templateUrl: './selected-cars.component.html',
  styleUrls: ['./selected-cars.component.css']
})
export class SelectedCarsComponent implements OnInit {

  @ViewChild('acForm') signupForm: NgForm;

  selectedCars: Car[] = [];
  carsWiners: Car[] = [];
  distance: number;
  isStarted = false;

  trafficLights: TrafficLight[];
  speedLimits: SpeedLimit[];
  allObsSubscriptions: Subscription[] = [];

  constructor(private carsService: CarsService, private sharedService: SharedService) {}

  ngOnInit() {
    // Subscibe to select cars method
    this.carsService.onSelectCars.subscribe((data: Car[]) => {
      this.selectedCars = data;
    });

    // Subscribe to get distance from api
    this.sharedService.getDistanceFromApi().subscribe((data) => {
      this.distance = data;
    });

    // Subscribe to get traffic lights from api
    this.sharedService.getTrafficLights().subscribe((trafficLights: TrafficLight[]) => {
      this.trafficLights = trafficLights;
    });

     // Subscribe to get spped limits from api
     this.sharedService.getSpeedLimits().subscribe((speedLimits: SpeedLimit[]) => {
      this.speedLimits = speedLimits;
    });
  }

  // Start all animations
  onStart() {
    if (this.signupForm.valid) {
      this.isStarted = true;
      let accelerateSpeed = 1;
      if (this.signupForm.value.accelerate !== '' && this.signupForm.form.controls.accelerate.valid) {
        // Get accelerateSpeed if entered
        accelerateSpeed = this.signupForm.value.accelerate;
      }
      // Subscribe to lights change - dont accelerate this animation, lights to fast
      this.subscribeToLightsChange(1);
      // Subscribe to cars move
      this.subscribeToCarsEngine(accelerateSpeed);

    } else if (this.signupForm.touched && !this.signupForm.valid) {
      swal({
        title: 'Please enter a number!',
        text: 'To accelerate speed you must enter a number from 1 to 99!',
        type: 'error',
        confirmButtonText: 'Ok'
      });
    }
  }

  // Stop all animations
  onStop() {
    this.isStarted = false;
    // Unsubscribe to lights change
    for ( const subscription of this.allObsSubscriptions) {
      subscription.unsubscribe();
    }

    // Reset selectedCars distance
    this.selectedCars.forEach((car, i) => {
      this.selectedCars[i].travelledDistanceInProcent = 0;
    });

    // Empty winners array
    this.carsWiners = [];
  }

  private subscribeToLightsChange(accelerateSpeed: number) {
    this.trafficLights.forEach((trafficLight, i) => {

      const period = Math.round(trafficLight.duration / accelerateSpeed);
      trafficLight.setLightsObs(Observable.interval(period));

      const subscription = trafficLight.getLightsObs().subscribe((x: number) => {
        this.trafficLights[i].color === 'red' ? this.trafficLights[i].color = 'green' : this.trafficLights[i].color = 'red';
      });

      this.allObsSubscriptions.push(subscription);
    });
  }

  private subscribeToCarsEngine(accelerateSpeed: number) {
    this.selectedCars.forEach((car, i) => {
      // Set AnimationSpeed
      const period = Math.round(this.getDurationInMs(car.getSpeed()) / accelerateSpeed);
      car.setCarObs(Observable.interval(period));
      const subscription = car.getCarObs().subscribe((x: number) => {
        // Unsubscribe if car finishe distance
        if (this.selectedCars[i].travelledDistanceInProcent === 100) {
          subscription.unsubscribe();
          // Push first 3 car in winners array
          if (this.carsWiners.length < 3) {
            this.carsWiners.push(this.selectedCars[i]);
          }
        }
        // Stop car if light is red
        for (const trafficLight of this.trafficLights) {
          if (trafficLight.position === this.selectedCars[i].travelledDistanceInProcent && trafficLight.color === 'red') {
            return false;
          }
        }
        this.selectedCars[i].travelledDistanceInProcent++;
      });
      this.allObsSubscriptions.push(subscription);
    });
  }

  private getDurationInMs(speed: number) {
    // calculate time in ms for 1% of distance
    // * 3600 -> convert h -> s * 1000 convert s -> ms /100 -> to get for 1%
    return Math.round((this.distance / speed) * 3600 * 10);
    // return (this.distance / this.speed) * 3600 * 1000; -> za celu putanju
  }
}
