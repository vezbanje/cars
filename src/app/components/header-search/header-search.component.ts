import { Car } from './../../models/car.model';
import { CarsService } from './../../services/cars/cars.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.css']
})
export class HeaderSearchComponent implements OnInit {

  public searchText: string;

  constructor(private carsService: CarsService) { }

  ngOnInit() {
  }

  // Call this method on submit search
  onSbmit() {
    this.carsService.searchCars(this.searchText);
  }

}
