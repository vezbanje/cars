import { Car } from './../../models/car.model';
import { CarsService } from './../../services/cars/cars.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css']
})
export class CarsListComponent implements OnInit {

  public cars: Car[];

  constructor(private carsService: CarsService) { }

  ngOnInit() {
    // Subscribe to cars get obs
    this.carsService.getCarsFromApi().subscribe((cars: Car[]) => {
      this.cars = cars;
    });

    // Subscribe to search cars
    this.carsService.onSearchCars.subscribe((data: Car[]) => {
      this.cars = data;
    });
  }
}
