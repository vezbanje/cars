import { CarsService } from './../../services/cars/cars.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @Input() car;

  constructor(private carsService: CarsService) { }

  ngOnInit() {
  }

  onSelectCar(id: number) {
    this.carsService.addCarToSelectedArray(id);
  }

}
