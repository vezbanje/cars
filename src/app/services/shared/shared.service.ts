import { TrafficLight } from './../../models/traffic-light.model';
import { SpeedLimit } from './../../models/speed-limit.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response} from '@angular/http';

@Injectable()
export class SharedService {

  private speedLimits: SpeedLimit[] = [];
  private trafficLights: TrafficLight[] = [];

  constructor(private htpp: Http) {}

  public getDistanceFromApi() {
    return this.htpp.get('https://cars-app-1981e.firebaseio.com/data/distance.json').map((response: Response) => {
      const data = response.json();
      return data;
    });
  }

  public getSpeedLimits() {
    return this.htpp.get('https://cars-app-1981e.firebaseio.com/data/speed_limits.json').map((response: Response) => {
      const data = response.json();
      for (const speedLimit of data) {
        this.speedLimits.push(new SpeedLimit(speedLimit.position, speedLimit.speed, speedLimit.image));
      }
      return this.speedLimits;
    });
  }

  public getTrafficLights() {
    return this.htpp.get('https://cars-app-1981e.firebaseio.com/data/traffic_lights.json').map((response: Response) => {
      const data = response.json();
      for (const trafficLight of data) {
        this.trafficLights.push(new TrafficLight(trafficLight.position, trafficLight.duration));
      }
      return this.trafficLights;
    });
  }
}
