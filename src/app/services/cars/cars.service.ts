import { element } from 'protractor';
import { Car } from './../../models/car.model';

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response} from '@angular/http';

import swal from 'sweetalert2';

@Injectable()
export class CarsService {

  private cars: Car[] = [];
  // Array of selected cars
  private selectedCars: Car[] = [];

  // Create subject to easy send data to cars-list component
  onSearchCars = new Subject();
  onSelectCars = new Subject();

  constructor(private htpp: Http) {
  }


  // Get cars from api service
  getCarsFromApi() {
    return this.htpp.get('https://cars-app-1981e.firebaseio.com/data/cars.json').map((response: Response) => {
      const carsData = response.json();
      for (const car of carsData) {
        this.cars.push( new Car(car.name, car.description, car.id, car.speed, car.image) );
      }
      return this.cars;
    });
  }

  // Filter cars by name
  public searchCars(name: string) {
    const cars = this.cars.filter((car: Car) => {
      return car.getName().toLowerCase().indexOf(name.toLowerCase()) === -1 ? false : true;
    });
    this.onSearchCars.next(cars);
  }

  // Add or remove cars in selected array of cars
  public addCarToSelectedArray(id: number) {
     const carIndex = this.getIndexById(id, this.cars);

    if (!this.selectedCars.includes(this.cars[carIndex])) {
      this.selectedCars.push(this.cars[carIndex]);
      this.onSelectCars.next(this.selectedCars);
    } else {
      swal({
      title: 'Already selected!',
        text: 'Do you want to remove from selected?',
        type: 'info',
        confirmButtonText: 'Yes',
        showCancelButton: true,
      }).then((answer) => {
        if (answer) {
          // get index from selected cars array
          const selectedIndex = this.getIndexById(id, this.selectedCars);
          this.selectedCars.splice(selectedIndex, 1);
          this.onSelectCars.next(this.selectedCars);
        }
      }).catch(() => {
        console.log('not do anything');
      });
    }
  }

  // Return index of element in car array by id
  private getIndexById(id: number, cars: Car[]): number {
    return cars.findIndex((car: Car) => {
      return car.getId() === id;
    });
  }

}
